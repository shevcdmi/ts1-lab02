package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class ShevcdmiTest {
    @Test
    public void factorial_nIs5_returns128() {
        // Arrange
        Shevcdmi shevcdmi = new Shevcdmi();
        // Act
        int actual = shevcdmi.factorial(5);
        // Assert
        Assertions.assertEquals(128, actual);


    }
    @Test
    public void factorialTest1() {
        // Arrange
        Shevcdmi shevcdmi1 = new Shevcdmi();
        // Act
        int actual1 = shevcdmi1.factorial(1);
        // Assert
        Assertions.assertEquals(1, actual1);
    }
}